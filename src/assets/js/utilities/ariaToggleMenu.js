"use strict"
import * as $ from "jquery"
import UI from "uikit"
    /**
     *
     * @since 1.0.0
     *
     * @author Digital Services <communications@det.nsw.edu.au>
     * @copyright © 2015 State Government of NSW 2015
     *
     * @class
     * @requires jQuery
     */

class ariaToggleMenu {


    /**
     * Script for Google translate feature
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     *
     */

    constructor(selector, config) {

        // Check if config has been passed to constructor
        if (config) {
            // Merge default config with passed config
            this.config = $.extend(true, {}, this.config, config)
        }

        /* UIKIT's function for dropdown open event 
          This extra bit of coding is needed so that the aria labels get set correctly in the anchor tag */
        $('[data-uk-dropdown]').on('show.uk.dropdown', function() {
            $(this).children('a').attr("aria-expanded", "true");
            $(this).find('.edu-dropdown-menu').removeAttr("aria-hidden");
            //adding the following conditions to fix tab issue in IE11
            //$(this).find('.edu-dropdown-menu').removeAttr('sizset').removeAttr('sizcache');
            //$(this).find('.edu-meganav-container').attr("aria-hidden", "true");
            //$(this).find('.edu-mega-nav-desc').attr("aria-hidden", "true");
            //$(this).find('.edu-meganav-container').focus();
            $(this).find('.edu-dropdown-menu').attr("tabindex","0");
            removeAriaAttributes(); 
        });
        /*  UIKIT's function for dropdown close event*/
        $('[data-uk-dropdown]').on('hide.uk.dropdown', function() {
            $(this).children('a').attr("aria-expanded", "false");
            removeAriaAttributes();
        });
        /* Removing unnecessary aria-tags in the li */
         $('.edu-meganav-megalinks').on('keyup', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 9) {
                $(this).parent().prev('.edu-parent-menu').removeClass('uk-open');
                $(this).parent().prev('.edu-parent-menu').removeAttr("aria-expanded");
                $(this).parent().prev('.edu-parent-menu').removeAttr("aria-haspopup");
                removeAriaAttributes();
            }
        });
        //identify the last link of the last submenu so that when the user tabs off the megamenu closes
        $('.edu-mega-menu-title-bar li.edu-parent-menu:last-child').addClass("edu-close-dropdown__lastlist");
        $('.edu-close-dropdown__lastlist li:last-child').addClass("edu-close-dropdown__lastlist-link")
        $('.edu-close-dropdown__lastlist-link').on('keydown', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which)
            if (code === 9) {
                $('.edu-close-dropdown__lastlist').removeClass('uk-open');
                $('.edu-close-dropdown__lastlist').removeAttr("aria-expanded");
                $('.edu-close-dropdown__lastlist').removeAttr("aria-haspopup");
                UIkit.dropdown('.uk-dropdown').hide();
                $('uk-dropdown').attr("aria-hidden", "true");
            }
        });
        $("li.uk-parent.edu-parent-menu").mouseover(function() {
            setTimeout(function() {
                $(this).each(function() {
                    removeAriaAttributes();
                });
            });
        });
        function removeAriaAttributes() {
            $('li.uk-parent.edu-parent-menu').removeAttr("aria-expanded");
            $('li.uk-parent.edu-parent-menu').removeAttr("aria-haspopup");
        }
    }

}

export default ariaToggleMenu