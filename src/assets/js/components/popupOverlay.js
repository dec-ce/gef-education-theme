"use strict"
import * as $ from "jquery"
import classManipulator from "utilities/classManipulator"

/**
* popupOverlay.js - This component creates a popup window with a dimmed background. Leverages the classManipulator utility.
*
* @since 0.1.09
*
* @author Digital Services <communications@det.nsw.edu.au>
* @copyright © 2016 State Government of NSW 2016
*
* @class
* @requires jQuery
*/

class PopupOverlay {

  //     @param {string} config.selector  - The selector that triggers popup window
  //     @param {string} config.event_type    - Type of event that triggers the popup window
  //     @param {string} config.class_name - A class to add to the popup window
  //     @param {string} config.subject     - The class of the div that will be the popup window
  //     @param {object} config.toggle - Toggles the popup window.
  //     @param {string} config.click_outside   - If you want the popup window to close when clicking outside the window.
  //     @param {string} config.insertBeforeElement    - Where in the DOM the background div element will be created. 
  //     @param {string} config.policy-popup-background  -  The class that will be attached to the dimmed background.

  constructor(selector, config) {

    // Set the selector
    this.$object = $(selector)

    // Instantiate the classManipulator
    this.popupWindow = new classManipulator(selector, config)

    //Element where the popup div will be inserted before
    var insertBefore = config.insertBeforeElement

    //Class of the popup background
    var backgroundClass = config.backgroundClass

    //Class of the popup window
    var popupClass = config.class_name

    //Class of the object you want to add popup window to
    var popupSubject = config.subject

    //Create a new div
    var popupObjectPosition = document.createElement("div")
    //Add a class to the new div create so we can locate it later
    popupObjectPosition.className = 'popupObjectPosition'

    //This stores data for elements that 'aria-hidden' is applied to.
    var bodyChildren = []; 

    //Load background
    this.popupBackground(insertBefore,backgroundClass,popupClass,popupSubject,popupObjectPosition,bodyChildren)


  }
    
  popupBackground(insertBefore,backgroundClass,popupClass,popupSubject,popupObjectPosition,bodyChildren){
    var toggleState = 'active' 

    this.$object.click(function(){

      if (toggleState == 'active'){

        // Give all non-modal top level elements an ARIA-hidden tag     
        $("body").children().each(function () {
        var jQchild = $(this);
        
        var o = {};
        o.jqel = jQchild;

        if(jQchild.attr("aria-hidden") !== undefined){
          o.hadVal = true;
          o.originalVal = jQchild.attr("aria-hidden");
        }else{
          o.hadVal = false;
        }
        bodyChildren.push(o);
        //hide the child
        jQchild.attr("aria-hidden", "true");
      });

        //Show dimmed background
        $('.atlwdg-blanket').show()
        $('.atlwdg-blanket').css('z-index', 12)

        //Add popup class to the object that is to become a popup
        $(popupSubject).addClass(popupClass)

        //Place the new div next to the popup div
        $(popupSubject).after(popupObjectPosition)

        //The target position of where we are going to move the popup elements
        var targetPosition = $( "div" ).last()

        // Move popup elements, we move this to the bottom of the page for accessibility tabbing.
        targetPosition.after($(popupSubject))

        //Focus on the top element when popup open
        var a = document.querySelector('.policy-enquires-main-content')
       a.getElementsByTagName('h2')[0].focus()

      $('.policy-enquires-main-content h2').on('focus', function() {
        $(this).bind('keydown', function(e) {
          if(e.shiftKey && e.keyCode == 9) { 
            return false;
          }
        });
      }); 

        toggleState = 'inactive'
      } 
    });

    //Save 'this' object so can be used within functions
    var _this = this     

    //Close popup when close icon button is clicked
    $('.close-popup-icon').bind('click', function(e) {
        if (toggleState == 'inactive'){
          if (e.keyCode == 13) { 
               _this.removePopup(popupClass,popupSubject,bodyChildren)           
               toggleState = 'active'
            } 
          _this.removePopup(popupClass,popupSubject,bodyChildren)           
          toggleState = 'active'
        }
    })

    //Return focus to first element of popup after tabbing from close button
      $('.close-popup-icon').on('focus', function() {
        $(this).bind('keydown', function(e) {
          
          // Prevent tab key going beyond close button, but still allowing shift + tab
          if (e.keyCode == 9) { 
            if (e.shiftKey) { 
              return true;
            }
            else{
              return false;
            }
          }  
        });
      }); 

    //Close popup when escape key is clicked
    $(document).keyup(function(e) {
      if (toggleState == 'inactive'){ 
        if (e.keyCode == 27) { // escape key maps to keycode `27`
          _this.removePopup(popupClass,popupSubject,bodyChildren) 
          toggleState = 'active' 
        }
      }
    })
  }

  removePopup(popupClass,popupSubject,bodyChildren){

    //Remove the background div element & popupbox
    $('.atlwdg-blanket').hide();
    $("." + popupClass).removeClass(popupClass);

    //Remove Popup Class
    $(popupSubject).removeClass(popupClass)

    //Remove popupObjectPosition element
    $('.popupObjectPosition').after($(popupSubject))
    $('.popupObjectPosition').remove();

    //Focus on element once window closed
    this.$object.focus()

    //Remove ARIA tags that were previously added by script
    for(var i=0, c; c=bodyChildren[i]; i++){
        //if element originally had aria-hidden attribute, reinstate value
        if(c.hadVal){
          c.jqel.attr("aria-hidden",c.originalVal);
        //if no original aria-hidden, remove the attribute entirely
        }else{
          c.jqel.removeAttr("aria-hidden");
        }
    }
  }
}

export default PopupOverlay

